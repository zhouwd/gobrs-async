package com.gobrs.async.core.timer;

import java.lang.ref.SoftReference;
import java.util.concurrent.ScheduledFuture;

public class TimerReference extends SoftReference<TimerListener> {
    private final ScheduledFuture<?> f;

    /**
     * Instantiates a new Timer reference.
     *
     * @param referent the referent
     * @param f        the f
     */
    TimerReference(TimerListener referent, ScheduledFuture<?> f) {
        super(referent);
        this.f = f;
    }

    @Override
    public void clear() {
        super.clear();
        // stop this ScheduledFuture from any further executions
        f.cancel(false);
    }

}
