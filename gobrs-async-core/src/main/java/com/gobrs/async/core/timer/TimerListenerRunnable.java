package com.gobrs.async.core.timer;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;

public class TimerListenerRunnable implements Runnable{

    private static final Logger logger = LoggerFactory.getLogger(TimerListenerRunnable.class);
    WeakReference<TimerListener> reference;
    public TimerListenerRunnable(TimerListener timerListener){
        reference = new WeakReference<>(timerListener);
    }
    @Override
    public void run() {
        try {
            TimerListener listener = reference.get();
            if(null!=listener) {
                listener.tick();
            }else{
                System.out.println("");
            }
        } catch (Exception e) {
            logger.error(Strings.EMPTY, e);
        }
    }
}
